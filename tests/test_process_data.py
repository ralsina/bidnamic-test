import pytest

import monitor.load_data

from monitor.load_data import process_data
from monitor.validators import ValidationError

GOOD_ROW = ("term", "s", "s", "s", "s", "12", "GBP", "1.5", "100", "s", "1.2")


def test_process_data_happy_path():
    """Test with good data."""
    row = GOOD_ROW
    value = process_data(row)
    assert value == ("GBP", ("term", 12, 1.5, 100, 1.2, 0.7999999999999999))


def test_process_data_invalid_data():
    """Test with invalid data. One error per validated field."""
    row = [None for _ in range(11)]
    with pytest.raises(ValidationError) as err:
        process_data(row)
    assert err.value.data == row
    assert err.value.errors == {
        "clicks": [
            "field 'clicks' cannot be coerced: 'NoneType' object has no attribute 'replace'",
            "null value not allowed",
        ],
        "conversion_value": [
            "field 'conversion_value' cannot be coerced: 'NoneType' object has no attribute 'replace'",
            "null value not allowed",
        ],
        "cost": [
            "field 'cost' cannot be coerced: 'NoneType' object has no attribute 'replace'",
            "null value not allowed",
        ],
        "impressions": [
            "field 'impressions' cannot be coerced: 'NoneType' object has no attribute 'replace'",
            "null value not allowed",
        ],
        "search_term": ["null value not allowed"],
    }


def test_process_data_short_row():
    """Short rows should error out."""
    row = GOOD_ROW[:5]
    with pytest.raises(ValidationError) as err:
        process_data(row)
    assert err.value.data == row
    assert err.value.errors == {"error": "Wrong number of fields (5, should be 11)"}


def test_process_data_long_row():
    """Long rows should error out."""
    row = GOOD_ROW * 2
    with pytest.raises(ValidationError) as err:
        process_data(row)
    assert err.value.data == row
    assert err.value.errors == {"error": "Wrong number of fields (22, should be 11)"}


def test_column_definitions(monkeypatch):
    """Which column has each data is configurable, test if that works."""
    # Make every field use column 3
    monkeypatch.setattr(monitor.load_data, "SEARCH_TERM", 3)
    monkeypatch.setattr(monitor.load_data, "CLICKS", 3)
    monkeypatch.setattr(monitor.load_data, "CURRENCY", 3)
    monkeypatch.setattr(monitor.load_data, "COST", 3)
    monkeypatch.setattr(monitor.load_data, "IMPRESSIONS", 3)
    monkeypatch.setattr(monitor.load_data, "CONVERSION_VALUE", 3)
    monkeypatch.setattr(monitor.load_data, "FIELD_COUNT", 5)

    row = [None for _ in range(5)]
    row[3] = "2"
    value = process_data(row)
    assert value == ("2", ("2", 2, 2.0, 2, 2.0, 1.0))
