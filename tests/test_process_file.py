import pytest

import monitor.load_data
from monitor.load_data import process_file

GOOD_ROW = ("term", "s", "s", "s", "s", "12", "GBP", "1.5", "100", "s", "1.2")


def test_process_file_happy_path(monkeypatch):
    """Test basic processing of good data."""

    def good_data(*_):
        for _ in range(5):
            yield GOOD_ROW

    # Mock load_file with something that yields good data
    monkeypatch.setattr(monitor.load_data, "load_file", good_data)
    value = list(process_file("foo"))

    expected_value = (True, "GBP", ("term", 12, 1.5, 100, 1.2, 0.7999999999999999))
    assert value == [expected_value for _ in range(5)]


def test_process_file_bad_data_mixed(monkeypatch):
    """Test mixed good/bad data."""

    def good_and_bad_data(*_):
        yield GOOD_ROW
        yield [None]
        yield GOOD_ROW
        yield GOOD_ROW

    # Mock load_file with something that yields good data
    monkeypatch.setattr(monitor.load_data, "load_file", good_and_bad_data)
    value = list(process_file("foo"))

    expected_value = (True, "GBP", ("term", 12, 1.5, 100, 1.2, 0.7999999999999999))
    assert value == [
        expected_value,
        (False, [None], {"error": "Wrong number of fields (1, should be 11)"}),
        expected_value,
        expected_value,
    ]
