from monitor.load_data import load_file


def test_load_file_basic():
    """Test that it yields rows parsing tab-separated file."""
    data = list(load_file("tests/fixtures/short.csv"))
    assert len(data) == 21  # File has 22 lines, skips first
    expected_value = [11 for _ in range(21)]
    expected_value[18] = 3  # There is one short row
    assert [len(row) for row in data] == expected_value
