from cerberus import Validator

THOUSANDS_SEPARATOR = ","


class ValidationError(Exception):
    def __init__(self, data, errors):
        super().__init__()
        self.data = data
        self.errors = errors


class RowValidator(Validator):
    """Validator specifically for this dataset."""

    def _normalize_coerce_integer_with_commas(self, value):
        """Normalize integers that look like 2,030"""
        return int(value.replace(THOUSANDS_SEPARATOR, ""))

    def _normalize_coerce_float_with_commas(self, value):
        """Normalize floats that look like 2,030.45"""
        return float(value.replace(THOUSANDS_SEPARATOR, ""))


def is_zero(field, value, error):
    if value == 0:
        error(field, "Invalid value 0")


row_schema = {
    "search_term": {"type": "string"},
    "clicks": {"type": "integer", "coerce": "integer_with_commas"},
    "cost": {"type": "number", "check_with": is_zero, "coerce": "float_with_commas"},
    "impressions": {"type": "integer", "coerce": "integer_with_commas"},
    "conversion_value": {"type": "number", "coerce": "float_with_commas"},
}

# singleton validator
row_validator = RowValidator(row_schema)
