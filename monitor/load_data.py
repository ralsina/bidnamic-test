"""Load data from tab-separated file (as provided in the example)"""

import csv

from .validators import ValidationError, row_validator

# Column keys
SEARCH_TERM = 0
CLICKS = 5
CURRENCY = 6
COST = 7
IMPRESSIONS = 8
CONVERSION_VALUE = 10

FIELD_COUNT = 11


def process_data(row):
    """Takes a row of data as provided in input, produces required output data.

    ROAS = conversion value / cost
    Required format: search_term, clicks, cost, impressions, conversion_value, roas
    Return value: (currency, (data))
    """

    if len(row) != FIELD_COUNT:
        raise ValidationError(
            row,
            {"error": f"Wrong number of fields ({len(row)}, should be {FIELD_COUNT})"},
        )

    data = dict(
        search_term=row[SEARCH_TERM],
        clicks=row[CLICKS],
        cost=row[COST],
        impressions=row[IMPRESSIONS],
        conversion_value=row[CONVERSION_VALUE],
    )

    if row_validator.validate(data):
        doc = row_validator.document
        roas = doc["conversion_value"] / doc["cost"]
        return (
            row[CURRENCY],
            (
                doc["search_term"],
                doc["clicks"],
                doc["cost"],
                doc["impressions"],
                doc["conversion_value"],
                roas,
            ),
        )
    else:
        # Invalid data, raise original raw data for analysis
        print(row_validator.errors)
        raise ValidationError(row, row_validator.errors.copy())


def process_file(path):
    """Get relevant metadata for file and process contents, yields timestamp,
    then tuples of (True, currency, data) or (False, currency, data, error)"""

    for row in load_file(path):
        try:
            currency, processed_row = process_data(row)
            yield (True, currency, processed_row)
        except ValidationError as err:
            yield (False, err.data, err.errors)


def load_file(path):
    """Parse input file, split rows in fields, yields rows."""

    with open(path, encoding="utf-16") as inf:
        reader = csv.reader(inf, delimiter="\t")
        next(reader)  # Heading line
        for line in reader:
            yield (line)


if __name__ == "__main__":
    import sys

    for row in process_file(sys.argv[1]):
        print(row)
