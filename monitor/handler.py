import csv
import logging
import os
import sys
from datetime import datetime

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from .load_data import process_file


def csv_writer_factory(timestamp, currency):
    path = os.path.join("processed", currency, "search_terms", timestamp + ".csv")
    os.makedirs(os.path.dirname(path), exist_ok=True)
    csv_file = open(path, "w", newline="")
    writer = csv.writer(csv_file)
    return writer


def csv_error_writer_factory(timestamp):
    path = os.path.join("processed", timestamp + "-errors.csv")
    os.makedirs(os.path.dirname(path), exist_ok=True)
    csv_file = open(path, "w", newline="")
    writer = csv.writer(csv_file)
    return writer


class EventHandler(FileSystemEventHandler):
    def on_created(self, event):
        super(EventHandler, self).on_created(event)

        if event.is_directory:
            logging.debug("Ignoring directory creation")
            return
        # Using modification date as timestamp, which is when we got it.
        # TODO: confirm this is correct per requirements
        logging.info(f"Processing file: {event.src_path}")
        timestamp = os.path.getmtime(event.src_path)
        # TODO: verify how this works with timezones!
        timestamp = datetime.utcfromtimestamp(timestamp).isoformat()
        logging.info(f"Timestamp: {timestamp}")
        processor = process_file(event.src_path)

        writers = {}
        error_writers = {}

        for record in processor:
            if record[0]:  # Valid record
                currency, data = record[1:]
                if currency not in writers:
                    # Create the writer
                    writers[currency] = csv_writer_factory(timestamp, currency)
                writers[currency].writerow(data)
            else:  # Invalid record
                if timestamp not in error_writers:
                    # Create the writer
                    error_writers[timestamp] = csv_error_writer_factory(timestamp)
                error_writers[timestamp].writerow(record[1:])

        # Move the source file to processed, renamed as timestamp.csv
        dest_path = os.path.join("processed", timestamp + ".csv")
        logging.info(f"Moving {event.src_path} => {dest_path}")
        os.rename(event.src_path, dest_path)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    path = sys.argv[1] if len(sys.argv) > 1 else "."
    event_handler = EventHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while observer.is_alive():
            observer.join(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
