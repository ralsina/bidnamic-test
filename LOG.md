# Bidnamic Test Development

## Start 2/20/20 8:37 PM

This is the original requirement text.

----

> **Data Ingestion Task**

> Our system ingests search term data from Google Adwords in CSV format and
> scores each search term with its Return On Ad Spend (ROAS).

> ROAS = conversion value / cost

> With the example search term CSV file provided write a python daemon that:

> 1) Monitors a directory for new csv files.
> 2) When a file arrives parse it and calculate the ROAS for each search term and
write out a new csv file.
> 3) Output file format :

> a) “processed/$currency/search_terms/$timestamp.csv” of the format:
search_term, clicks, cost, impressions, conversion_value, roas

> Consider how to handle corrupt files or individual rows and a high throughput
scenario.

> Also consider how to handle Adwords API Errors and Throttling.

> In the follow up interview you will require to demo your system working and talk
> through the solution, possible pitfalls and other considerations or approaches.

----

2/20/20 8:42 PM

A sample data file is provided "Search terms report.csv", 275MB, 660K rows.

Doesn't seem to be comma-separated, more likey tabs or spaces.

Quick test looks like tabs are consistently used:

``head -100 Search\ terms\ report.csv | cut -d\t -f3``

----

2/20/20 8:45 PM

Initialized repo at https://gitlab.com/ralsina/bidnamic-test, added basic gitignore, empty requirements.txt

----

From [Nikola](https://getnikola.com) I recall using watchdog to monitor directories for events, so let's use that. Adding torequirements, writing very basic daemon to monitor a folder passed as
argument.

----

2/20/20 8:56 PM Dinner

----

2/20/20 9:02 PM That was quick :-)

Reading watchdog docs, found the [quickstart example](https://python-watchdog.readthedocs.io/en/v0.10.2/quickstart.html#quickstart) it's pretty much all I need out of it.

Seems to work, detects too much:

```
$  python monitor.py input
2020-02-20 21:06:01 - Created file: input/fo
2020-02-20 21:06:01 - Modified directory: input
2020-02-20 21:06:01 - Modified file: input/fo
```

(committed)

----

Will read the docs and limit event detection to just file creation. As long as files are atomically created that should work.

Also, will wire a dummy function as observer instead of the logging handler.

(committed)

-----

2/20/20 9:17 PM Starting on parsing input file.

Tempting to just throw pandas at it, but if we have to handle per-line broken input ... that may not be a good idea. Let's start with the simplest possible version using the csv module from stdlib.

Looking at the official [csv docs](https://docs.python.org/3/library/csv.html) because I never remember how it works.

```
UnicodeDecodeError: 'utf-8' codec can't decode byte 0xff in position 0: invalid start byte
```

Oh, well. It's UTF-16???? Sure, why not.

A quick sweep of the file shows all rows have 11 elements, which is good. Have basic CSV reader in place (doesn't do anything with the data yet)

(commit)

----

2/20/20 9:31 PM Starting to connect things, will make the FS event handler trigger a function that will collect the required metadata from the input file, process, calculate, etc.

2/20/20 9:36 PM **NOTE** Output is required to be processed/**currency**/search_terms/timestamp.csv so it's important that there is either only one currency per input file (as in the example) or we classify accordingly. Just in case, let's classify.

2/20/20 9:50 PM Implemented naïve version of transform, of course it explodes because of bad data.

Let's do data validation.

2/20/20 9:53 PM Considering using a real validation tool, like Cerberus or something ... looks like crazy overkill for this task, where I just want numbers / non-zero.

2/20/20 9:58 PM Started on just doing things like "try: float(s)" but .... that would approve of "123x" as a number, which is just wrong, and also not easy to get totally right. 

So, cerberus it is, even if it's wildly overqualified for this.

2/20/20 10:21 PM Good thing about Cerberus, it can do both validation and coercion, so it
does make things look nice once the schema is in place.

2/20/20 10:31 PM Implemented basic validation / coercion. Data fails because of "justifiable quirks" such as using "2,030" to mean 2030. Adding normalization for that case via a custom coercer (see https://docs.python-cerberus.org/en/stable/customize.html)

2/20/20 10:45 PM Implemented comma removal for integer coercion (committed) but the same thing happens for floats:

```
"field 'conversion_value' cannot be coerced: could not convert string to float: '1,278.96'", 'must be of number type'
```

2/20/20 10:49 PM Done for floats, test run to see if there are other data errors except for 0 cost (which I can't fix)

2/20/20 10:54 PM Apparently all data is processed correctly now, except we have items with 0 cost, which means ROAS can't be calculated. On the other hand this is now very slow (4 minutes!) so looks like the coercers need tweaking.

2/20/20 11:00 PM A quick attempt at making them faster makes no difference so reverting and making a note about possible performance issues.

----

2/20/20 11:01 PM So, only data problem left is that some rows have 0 cost. For now, let's consider those rows "failed" and give them NaN roas. This would have to be discussed tho, because in some cases it may still be salvageable. Example:

If conversion value and cost are both 0, then maybe ROAS should be defined as 0 since it's undefined? Or maybe cost=0 means it actually has a cost of 0 and ROAS should be an arbitrary number. I don't know enough to know.

So, let's use it as an opportunity to figure out what to do with "lines too broken for the current version". I'll put them in a side file with annotated validation errors.

----

2/20/20 11:04 PM Starting to close the loop, working on output.

2/20/20 11:20 PM Basic roundtrip done (prints to terminal)

2/20/20 11:34 PM Working on writers for processed data

2/20/20 11:37 PM Using this to convert unix timestamps to iso8601 (need to check timezone support!)

2/20/20 11:59 PM Basic functionality in place:

* Monitors folder
* Reads the data file
* Parses it
* Processes valid rows
* Calculates ROAS for valid rows
* Puts valid data in the right place
* Supports multi-currency files (maybe not needed, easy to remove!)
* Puts invalid rows with error messages in specific place, easy to correlate with valid and source data
* Moves source data to specific place easy to correlate with valid/invalid data

Still TODO:

* Lots of tests
* Some python housekeeping ("packaging")
* CI setup
* Graceful handling of things like REALLY invalid rows (example: too few/too many fields)

But, midnight. That can wait for morning

------------

2/21/20 9:24 AM

Testing file with really broken line (too few fields), adding error handling.

2/21/20 9:45 AM

Reorganized code as a python package

2/21/20 9:47 AM

Changing prints into logging calls

2/21/20 10:11 AM

CI pipeline working with a dummy test

2/21/20 10:38 AM

Basic tests in place for process_data

2/21/20 10:48 AM

Basic tests for process_file in place

2/21/20 10:54 AM

Basic test for load_file in place