# Test task for Bidnamic

To run it, after setting up a virtualenv with the requirements:


```
python -m monitor folder_to_monitor
```

Copy/move a file with the data to be processed into that folder.

---------

To run unit tests:

```
env PYTHONPATH=. pytest -vv
```